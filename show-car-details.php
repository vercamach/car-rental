<?php
$sCarId = $_GET['id'] ?? '';

$sData = file_get_contents('data/cars.json');
$jData = json_decode($sData);
$jInnerData = $jData->data;
$jCar = $jInnerData->$sCarId
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>Car-rent</title>
</head>
      <body>

     <div id="mobile-menu">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&#9776;</a>
                <div class="nav-links">
                    <a class="active" href="new-booking.html" class="white" onclick="closeNav()" >New booking</a>
                    <a onclick="closeNav()" href="my-bookings.html">My boookings</a>
                    <a onclick="closeNav()" href="#projects">Frederik<i class="arrow down"></i></a>

                    <a href="index.html" onclick="closeNav()">Log out</a>
                </div>
            </div>

            <span id="openNav" onclick="openNav() ">&#9776;</span>
        </div>
        <header>
            <a href="index.html"> <img src="img/car-logo.png" class="logo-car" alt="logo-car"></a>
            <div class="header-nav">
                <a href="index.html">
                    <p class="nav">Log out</p>
                </a>
                <p class="nav">Frederik  <i class="arrow down"></i></p>
                <p class="nav">Contact us</p>
                <a href="my-bookings.html">
                    <p class="nav ">My bookings</p>
                </a>
                <a href="new-booking.html"> <p class="nav active ">New booking</p></a>
            </div>
        </header>
<div class="wrapper">
    <section>
      <div class='InfoContainer'>
        <div><h1 class="title">Choose vehicle</h1>
        <p class="thin">
            STEP 2: Browse available vehicles and choose the one that meets your needs.
        </p>
        </div>
        <div class='reservationBox'>
  <h4>YOUR BOOKING DETAILS</h4>
  <div class='bookingInfoDisplay'>
  <div >
    <div><h5 class="confirm-title"><img class="icon loc-icon" src="img/location.png" alt="">Pick-up</h5></div>
    <div>
       <div class='bookingDetails'>30/06/2019, 06:30</div>
       <div class='bookingDetails'>Copenhagen Airport </div>
     </div>
  </div>
  <div >
    <div><h5 class="confirm-title"><img class="icon loc-icon" src="img/location.png" alt="">Return</h5></div>
    <div>
       <div class='bookingDetails'>10/07/2019, 16:30</div>
       <div class='bookingDetails' > Odense </div>
    </div>
  </div>
  </div>
  <button class='new-booking yellow small' onClick="parent.location='new-booking.html'">Change</button>
</div>
</div>
<?php

echo "
  <div class='InfoContainer car'>
  <div>
  <img class='carInfoImg' src=$jCar->image >
  </div>
  <div class='carInfoRightContainer'>

<div class='flexTemplate'>
<h3>$jCar->name </h3>
<div class='priceBox'>
Rental price:
<h3>$jCar->price DDK</h3>
</div>
</div>
<div class='gridTemplate'>
<div class='specificationss'>
<p class='specs'><img class='icon' src='img/person.png' alt=''>5 seats
</p>
<p class='specs'><img class='icon' src='img/door.png' alt=''>4 doors</p>
<p class='specs'><img class='icon' src='img/suitcase.png' alt=''>Fits 3 suitcases</p>
<p class='specs'><img class='icon' src='img/checked.png' alt=''>Air condition</p>
<p class='specs'><img class='icon' src='img/checked.png' alt=''>Automatic</p>
</div>
<div class='specificationss'>
<p class='specs'><img class='icon' src='img/checked.png' alt=''>Reserve now, pay later</p>
<p class='specs'><img class='icon' src='img/checked.png' alt=''>Free Cancellation</p>
<p class='specs'><img class='icon' src='img/checked.png' alt=''>Unlimited free kilometers incl.</p>

<div class=' stars'>
<span class='fa fa-star checked icon'></span>
<span class='fa fa-star checked icon'></span>
<span class='fa fa-star checked icon'></span>
<span class='fa fa-star checked icon'></span>
<span class='fa fa-star icon'></span>


</div >
<div><button class='moreInfo'>Read reviews >> </button> </div>
</div>
</div>

</div>
</div>
<div class='car description'>
<h3>Description: </h3>
<p class='descriptionText'>$jCar->description</p>
</div>
  "

?>
<div class='flexTemplate'>
<button onclick= "window.history.back()" class=" yellow continue">Back</button>
<button class='car-list yellow continue' onClick="parent.location='extras.html'" >Select</button>
        </div>
</section>
<footer class="contact-us ">
                <div class="mobile-footer top-mob-footer">
                        <img class="contact-logo" src="img/phone.png" alt="phone-icon">
                        <p class="contact-text">40 39 28 37
                        </p>
                    </div>
                    <div class="mobile-footer">
                      <img class="contact-logo" src="img/unicorn-logo.png" alt="unicorn-icon">
                        <p class="contact-text">unicorn-rental@gmail.com
                        </p>
                    </div>

                <div class="phone">
                    <p class="white-text"><img class="contact-logo" src="img/phone.png" alt="phone-icon">Call us: </p>
                    <p class="contact-text">40 39 28 37
                    </p>
                </div>
                <div class="e-mail">
                    <p class="white-text"><img class="contact-logo" src="img/unicorn-logo.png" alt="unicorn-icon">Or send us an e-mail:</p>
                    <p class="contact-text">unicorn-rental@gmail.com
                    </p>
                </div>
                <div class="social-div">
                    <p class="white-text">Keep it social</p>
                    <div class="social"><img  src="img/facebook-logo.png" alt="">
                        <img src="img/twitter.png" alt="">
                        <img src="img/instagram.png" alt=""></div>

                    </div>

        </footer>
        </div>
        <script src="script.js"></script>
</body>
</html>