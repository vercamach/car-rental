document.querySelectorAll("#today").forEach(date => date.valueAsDate = new Date());

function openNav() {
    document.getElementById("mySidenav").style.height = "100%";
    document.getElementById("openNav").classList.add("hidden");

}

function closeNav() {
    document.getElementById("mySidenav").style.height = "0";
    document.getElementById("openNav").classList.remove("hidden");

}

function showModal() {
    let modal = document.getElementById("new-date-time-repeat")
    modal.classList.toggle("hidden");
    document.getElementById("continue-button").classList.toggle("hidden")
    document.getElementById("duplicate-button").classList.add("hidden")

}